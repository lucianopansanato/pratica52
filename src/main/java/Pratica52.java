
import utfpr.ct.dainf.if62c.pratica.Equacao2Grau;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica52 {
    public static void main(String[] args) {
        //System.out.println("Olá, Java!");
        Integer a = 1;
        Integer b = -2;
        Integer c = -3;
        
        // Caso em que as duas raízes são diferentes
        try {
            Equacao2Grau<Integer> e1 = new Equacao2Grau<>(a, b, c);
            System.out.println("x1 = " + e1.getRaiz1());
            System.out.println("x2 = " + e1.getRaiz2());
        } catch (Exception e) {
            System.out.println("Ocorreu um erro inesperado: "
            + e.getLocalizedMessage());
        }
        
        // Caso em que as duas raízes são iguais
        a = 9;
        b = -12;
        c = 4;
        try {
            Equacao2Grau<Integer> e2 = new Equacao2Grau<>(a, b, c);
            System.out.println("x1 = " + e2.getRaiz1());
            System.out.println("x2 = " + e2.getRaiz2());
        } catch (Exception e) {
            System.out.println("Ocorreu um erro inesperado: "
            + e.getLocalizedMessage());
        }    
        
        // Caso em que não há raízes
        a = 5;
        b = 3;
        c = 5;
        try {
            Equacao2Grau<Integer> e3 = new Equacao2Grau<>(a, b, c);
            System.out.println("x1 = " + e3.getRaiz1());
            System.out.println("x2 = " + e3.getRaiz2());
        } catch (Exception e) {
            System.out.println("Ocorreu um erro inesperado: "
            + e.getLocalizedMessage());
        }            
        
        // Caso em que a equação não é de segundo grau
        a = 0;
        b = -2;
        c = -3;
        try {
            Equacao2Grau<Integer> e4 = new Equacao2Grau<>(a, b, c);
            System.out.println("x1 = " + e4.getRaiz1());
            System.out.println("x2 = " + e4.getRaiz2());
        } catch (Exception e) {
            System.out.println("Ocorreu um erro inesperado: "
            + e.getLocalizedMessage());
        }            
    }
}
